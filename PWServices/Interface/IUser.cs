﻿using PWDomain;
using PWDomain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWServices.Interface
{
   public interface IUser
    {
         void AddUser(UserViewModel model);

        IEnumerable<User> GetUsers();

        UserViewModel GetUser(int Id);

        bool DeleteUser(int Id);
        bool CheckUserAuthentication(string email, string password);
    }
}
