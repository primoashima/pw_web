﻿using PWServices.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PWDomain;
using PWDomain.Models;
using System.Web.Security;
using System.Web;

namespace PWServices.Services
{
    public class UserService :  IUser
    {
        PW_WebEntities _entities = new PW_WebEntities();

        public void AddUser(UserViewModel model)
        {
            User obj = new User();
            if (model.Id!=0)
            {
                obj = _entities.Users.FirstOrDefault(x=> x.UserId == model.Id);
            }
            obj.First_Name = model.fname;
            obj.Last_Name = model.lname;
            obj.FKRoleId = model.RoleId;
            obj.Email = model.email;
            obj.Password = model.password;
            obj.Phone = model.Phone;
            obj.CreatedOn = DateTime.Now;
            obj.IsActive = true;           
            if (model.Id == 0)
            {
                _entities.Users.Add(obj);
            }
             _entities.SaveChanges();
        }


        public UserViewModel GetUser(int Id)
        {
            UserViewModel obj = new UserViewModel();
            var User = _entities.Users.FirstOrDefault(x=>x.UserId == Id);
            obj.Id = User.UserId;
            obj.fname = User.First_Name;
            obj.lname = User.Last_Name;
            obj.email = User.Email;
            obj.password = User.Password;
            obj.Phone = User.Phone;
            return obj;
        }

        public User CheckUserAuthentication(string email, string password)
        {
            var IsExist = this._entities.Users.FirstOrDefault(x => x.Email.ToLower() == email.ToLower() && x.Password == password && x.IsDeleted != true);
            if (IsExist != null)
            {
                return IsExist;
            }
            return null;
        }

        public IEnumerable<User> GetUsers()
        {
            var Users = _entities.Users.ToList().Where(x=> x.IsDeleted != true);
            return Users;
        }

        public bool DeleteUser(int Id)
        {
            var User = _entities.Users.FirstOrDefault(x=> x.UserId == Id);
            User.IsDeleted = true;
            _entities.SaveChanges();
            return true;
        }
        bool IUser.CheckUserAuthentication(string email, string password)
        {
            throw new NotImplementedException();
        }
    }
}
