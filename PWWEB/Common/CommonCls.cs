﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PWWEB.Common
{
    public class CommonCls
    {
        public static void CreateUserCookie(string user)
        {
            HttpContext.Current.Session["user"] = user;
        }
    }
}