﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PWWEB.Models
{
    public class DashboardViewModel
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public string Phone { get; set; }
    }

    public class UserInfo
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Role { get; set; }
        public string Email { get; set; }

        public int? RoleId { get; set; }
        public string Phone { get; set; }
    }
}