﻿using PWDomain.Models;
using PWServices.Services;
using PWWEB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace PWWEB.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
        UserService _userservice = new UserService();
        // GET: Dashboard
        public ActionResult Index()
        {
            return View();
        }
     
       
        public ActionResult AddUser(int Id)
        {
            ViewBag.Id = Id;
            if (Id!=0)
            {
                var User = _userservice.GetUser(Id);
            }
            return View(User);
        }
        [HttpPost]
        public ActionResult AddUser(UserViewModel model)
        {
          var valid =  ModelState.IsValid;
            _userservice.AddUser(model);
            return View();
        }

        public JsonResult Delete(int Id)
        {
            _userservice.DeleteUser(Id);
            return Json("success",JsonRequestBehavior.AllowGet);
        }
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Viewuser()
        {
           var usrs = _userservice.GetUsers();
            List<UserViewModel> list = new List<UserViewModel>();
            foreach (var item in usrs)
            {
                UserViewModel mode = new UserViewModel();
                mode.fname = item.First_Name;
                mode.lname = item.Last_Name;
                mode.email = item.Email;
                mode.password = item.Password;
                mode.Phone = item.Phone;
                list.Add(mode);
            }
            return View(list);
        }
        public ActionResult UserList()
        {
            var usrs = _userservice.GetUsers();
            List<UserViewModel> list = new List<UserViewModel>();
            foreach (var item in usrs)
            {
                UserViewModel mode = new UserViewModel();
                mode.Id = item.UserId;
                mode.fname = item.First_Name;
                mode.lname = item.Last_Name;
                mode.email = item.Email;
                mode.password = item.Password;
                mode.Phone = item.Phone;
                mode.Role = item.Role!= null? item.Role.RoleName : null;
                list.Add(mode);
            }
            return View(list);         
        }
        public ActionResult AddNewUser(int Id=0)
        {
            var user = new UserViewModel(); 
            if (Id != 0)
            {
                 user = _userservice.GetUser(Id);
                return View(user);
            }
            return View(user);
        }

      //  public UserInfo _UserInfo { get; set; }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.Identity is FormsIdentity)
                {
                    var id = (FormsIdentity)User.Identity;
                    FormsAuthenticationTicket ticket = id.Ticket;
                    string[] userData = ticket.UserData.Split(',');
                    var user = new UserInfo()
                    {
                        Id = Convert.ToInt32(userData[0]),
                        Email = userData[1],
                    };
                    _UserInfo = user;
                }
            }
        }

    }
}