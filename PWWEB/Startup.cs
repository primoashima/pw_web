﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PWWEB.Startup))]
namespace PWWEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
