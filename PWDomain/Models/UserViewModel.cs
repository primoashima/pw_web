﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PWDomain.Models
{
   public class UserViewModel
    {
        public int Id { get; set; }
        [Required]
        public string fname { get; set; }
        public string lname { get; set; }
        public string email { get; set; }
        public string password { get; set; }

        public string Phone { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
    }


}
